<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Admin;

class AdminController extends Controller
{
    //Read
    public function awal(){
    	$admin = Admin::all();
    	return view('admin.app', compact('admin'));
    }

    public function tambah(){
    	return view('admin.tambah');
    }
    //Create
    public function simpan(Request $input){

        $this->validate($input,[
     'username' => 'required',
     'password' => 'required',
     'nama' => 'required',
     'notlp' => 'required',
     'email' => 'required',
     'alamat'’ => 'required',
]);
    	$admin = new Admin();
    	$admin->nama = $input->nama;
    	$admin->notlp = $input->notlp;
    	$admin->email = $input->email;
    	$admin->alamat = $input->alamat;
        $admin->pengguna_id = $input->pengguna_id;
    	$status = $admin->save();
    	return redirect('admin')->with(['status'=>$status]);
    }

    public function edit($id){
    	$admin = Admin::find($id);
    	return view('admin.edit')->with(array('admin'=>$admin));
    }
    //Update
    public function update($id, Request $input){
    	$admin = Admin::find($id);
    	$admin->nama = $input->nama;
    	$admin->notlp = $input->notlp;
    	$admin->email = $input->email;
    	$admin->alamat = $input->alamat;
        $admin->pengguna_id = $input->pengguna_id;
    	$status = $admin->save();
    	return redirect('admin')->with(['status'=>$status]);
    }
    //Delete
    public function hapus($id){
    	$admin = Admin::find($id);
    	$admin->delete();
    	return redirect('admin');
    }
    //belum tau ini buat apa?
    public function lihat()
    {

    }
}
