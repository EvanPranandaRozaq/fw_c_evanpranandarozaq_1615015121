<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Buku;

class BukuController extends Controller
{
   	//Read
    public function awal(){
    	$buku = Buku::all();
    	return view('buku.app', compact('buku'));
    }

    public function tambah(){
    	return view('buku.tambah');
    }
    //Create
    public function simpan(Request $input){
        //buat validasi
         $this->validate($input,[
            'judul'=>'required',
            'penerbit'=>'required',
            'tanggal'=>'required',
            'kategori_id'=>'required',
        ]);

    	$buku = new Buku();
    	$buku->judul = $input->judul;
    	$buku->penerbit = $input->penerbit;
    	$buku->tanggal = $input->tanggal;
    	$buku->kategori_id = $input->kategori_id;
    	$status = $buku->save();
    	return redirect('buku')->with(['status'=>$status]);
    }

    public function edit($id){
    	$buku = Buku::find($id);
    	return view('buku.edit')->with(array('buku'=>$buku));
    }
    //Update
    public function update($id, Request $input){
    	$buku = Buku::find($id);
        $buku->judul = $input->judul;
        $buku->penerbit = $input->penerbit;
        $buku->tanggal = $input->tanggal;
    	$buku->kategori_id = $input->kategori_id;
    	$status = $buku->save();
    	return redirect('buku')->with(['status'=>$status]);
    }
    //Delete
    public function hapus($id){
    	$buku = Buku::find($id);
    	$buku->delete();
    	return redirect('buku');
    }
}
