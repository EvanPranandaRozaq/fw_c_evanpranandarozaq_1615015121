<div class="form-group">
	@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
	@endif 
</div>
<div class="form-group">
	<label class="col-sm-2">Deskripsi Buku</label>
	<div class="col-sm-9">
		{!! Form::textarea('deskripsi',null,['class'=>'form-control','placeholder'=>"Deskripsi Buku", 'required']) !!}
	</div>
</div>
