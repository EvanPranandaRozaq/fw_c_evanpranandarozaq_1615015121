<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
 //   return view('welcome');
//});
Route::get('/pengguna','PenggunaController@simpan');


//Buku
Route::get('/buku','BukuController@awal');
Route::get('/tambah','BukuController@lihat');
Route::get('/tambah/buku','BukuController@tambah');
Route::post('/buku/simpan','BukuController@simpan');
//buat hapus sama edit blom ada atau emang gaada?

//Penulis
Route::get('/tambah/penulis','PenulisController@tambah');
Route::post('/penulis/simpan','PenulisController@simpan');
Route::get('/penulis','PenulisController@awal');
Route::get('/penulis/edit/{penulis}','PenulisController@edit');
Route::post('/penulis/update/{penulis}','PenulisController@update');
Route::get('/penulis/hapus/{penulis}','PenulisController@hapus');
//Kategori
Route::get('kategori/tambah','KategoriController@tambah');
Route::post('/kategori/simpan','KategoriController@simpan');
Route::get('/kategori/edit/{kategori}','KategoriController@edit');
Route::get('/kategori','KategoriController@awal');
Route::post('/kategori/update/{kategori}','KategoriController@update');
Route::get('/kategori/hapus/{kategori}','KategoriController@hapus');
//Pembeli
Route::get('/pembeli/tambah','PembeliController@tambah');
Route::post('/pembeli/simpan','PembeliController@simpan');
Route::get('/pembeli/edit/{pembeli}','PembeliController@edit');
Route::get('/pembeli','PembeliController@awal');
Route::post('pembeli/update/{pembeli}','PembeliController@update');
Route::get('pembeli/hapus/{pembeli}','PembeliController@hapus');
//Admin
Route::get('/admin','AdminController@awal');
Route::post('/admin/simpan','AdminController@simpan');
Route::get('/admin/edit/{admin}','AdminController@edit');
Route::post('admin/update/{admin}','AdminController@update');
Route::get('admin/hapus/{admin}','AdminController@hapus');
Route::get('admin/lihat','AdminController@lihat');//ini buat apa?




